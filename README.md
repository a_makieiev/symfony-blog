# Symfony blog (PHP7-FPM - NGINX - MySQL)

## Installation

1. Create a `.env` from the `.env.dist` file. Adapt it according to your symfony application

    ```bash
    cp .env.dist .env
    ```


2. Build/run containers with (with and without detached mode)

    ```bash
    $ docker-compose build
    $ docker-compose up -d
    ```

3. Update your system host file (add symfony.local)

```bash
UNIX only: get containers IP address and update host (replace IP according to your configuration) (on Windows, edit C:\Windows\System32\drivers\etc\hosts)
$ sudo echo $(docker network inspect bridge | grep Gateway | grep -o -E '[0-9\.]+') "symfony.local" >> /etc/hosts
```

4. Prepare Symfony app
    1. Update php-code/.env

```
Configure your db driver and server_version in config/packages/doctrine.yaml
DATABASE_URL=mysql://user:userpass@db/mydb
```

    2. Composer install & create database

        ```
        
        $ docker-compose exec php bash
        
        $ composer install
        
        $ php bin/console doctrine:database:create
        
        $ php bin/console doctrine:schema:update --force
        
        $ php bin/console doctrine:fixtures:load --no-interaction
        
        ```


```bash
# bash commands
$ docker-compose exec php bash

# Composer (e.g. composer update)
$ docker-compose exec php composer update

# SF commands (Tips: there is an alias inside php container)
$ docker-compose exec php php /var/www/symfony/app/console cache:clear # Symfony2
$ docker-compose exec php php /var/www/symfony/bin/console cache:clear # Symfony3
# Same command by using alias
$ docker-compose exec php bash
$ sf cache:clear

# Retrieve an IP Address (here for the nginx container)
$ docker inspect --format '{{ .NetworkSettings.Networks.dockersymfony_default.IPAddress }}' $(docker ps -f name=nginx -q)
$ docker inspect $(docker ps -f name=nginx -q) | grep IPAddress

# MySQL commands
$ docker-compose exec db mysql -uroot -p"root"

# Chmod cache/logs folder
$ sudo chmod -R 777 var/cache var/logs var/sessions 

# Check CPU consumption
$ docker stats $(docker inspect -f "{{ .Name }}" $(docker ps -q))

# Delete all containers
$ docker rm $(docker ps -aq)

# Delete all images
$ docker rmi $(docker images -q)
```

# API DOCS 
https://documenter.getpostman.com/view/2008597/symfony-blog/RVu1GAEj#90010a5d-650b-f8ea-2baf-5fac28204e05
