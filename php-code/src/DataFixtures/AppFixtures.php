<?php
/**
 * Created by PhpStorm.
 * User: makeey
 * Date: 30.03.18
 * Time: 17:26
 */

namespace App\DataFixtures;


use App\Entity\Category;
use App\Entity\Post;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AppFixtures extends Fixture
{

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     * @throws \Doctrine\Common\DataFixtures\BadMethodCallException
     * @throws \Exception
     */
    public function load(ObjectManager $manager)
    {
        $this->UserLoad($manager);
        $this->CategoryLoad($manager);
        $this->PostLoad($manager);
    }

    /**
     * @param ObjectManager $manager
     * @throws \Doctrine\Common\DataFixtures\BadMethodCallException
     */
    public function CategoryLoad(ObjectManager $manager)
    {
        for ($i = 0; $i < 5; $i++) {
            $category = new Category('Title '.($i+1));
            $manager->persist($category);
            $this->addReference('Title '.($i+1), $category);
        }
        $manager->flush();
    }

    /**
     * @param ObjectManager $manager
     */
    public function PostLoad(ObjectManager $manager){
        for ($i = 0 ; $i < 10; $i++){
            $post = new Post();
            $post->setTitle($this->getRandomText());
            $post->setPublishedAt();
            $post->setContent($this->getRandomText());
            $post->setCategory($this->getReference('Title '. rand(1,5)));
            $post->setAuthor($this->getReference('admin'));
            $manager->persist($post);
        }
        $manager->flush();
    }

    /**
     * @param ObjectManager $manager
     * @throws \Doctrine\Common\DataFixtures\BadMethodCallException
     * @throws \Exception
     */
    public function UserLoad(ObjectManager $manager)
    {
        foreach ($this->UserData() as $data) {
            $user = new User();
            $user->setUsername($data['username']);
            $user->setPassword($data['password']);
            $user->setEmail($data['email']);
            $user->setRoles($data['role']);
            $user->setToken();
            $manager->persist($user);
            $this->addReference($data['username'], $user);
        }
        $manager->flush();
    }

    private function UserData(): array
    {
        return [
            ['username' => 'admin', 'email' => 'admin@expample.com', 'password' => 'admin', 'role' => ['ROLE_ADMIN','ROLE_USER']],
            ['username' => 'user', 'email' => 'user@expample.com', 'password' => 'user', 'role' => ['ROLE_USER']],
        ];
    }

    private function getPhrases(): array
    {
        return [
            'Lorem ipsum dolor sit amet consectetur adipiscing elit',
            'Pellentesque vitae velit ex',
            'Mauris dapibus risus quis suscipit vulputate',
            'Eros diam egestas libero eu vulputate risus',
            'In hac habitasse platea dictumst',
            'Morbi tempus commodo mattis',
            'Ut suscipit posuere justo at vulputate',
            'Ut eleifend mauris et risus ultrices egestas',
            'Aliquam sodales odio id eleifend tristique',
            'Urna nisl sollicitudin id varius orci quam id turpis',
            'Nulla porta lobortis ligula vel egestas',
            'Curabitur aliquam euismod dolor non ornare',
            'Sed varius a risus eget aliquam',
            'Nunc viverra elit ac laoreet suscipit',
            'Pellentesque et sapien pulvinar consectetur',
            'Ubi est barbatus nix',
            'Abnobas sunt hilotaes de placidus vita',
            'Ubi est audax amicitia',
            'Eposs sunt solems de superbus fortis',
            'Vae humani generis',
            'Diatrias tolerare tanquam noster caesium',
            'Teres talis saepe tractare de camerarius flavum sensorem',
            'Silva de secundus galatae demitto quadra',
            'Sunt accentores vitare salvus flavum parses',
            'Potus sensim ad ferox abnoba',
            'Sunt seculaes transferre talis camerarius fluctuies',
            'Era brevis ratione est',
            'Sunt torquises imitari velox mirabilis medicinaes',
            'Mineralis persuadere omnes finises desiderium',
            'Bassus fatalis classiss virtualiter transferre de flavum',
        ];
    }

    private function getRandomText(int $maxLength = 255): string
    {
        $phrases = $this->getPhrases();
        shuffle($phrases);

        while (mb_strlen($text = implode('. ', $phrases) . '.') > $maxLength) {
            array_pop($phrases);
        }

        return $text;
    }
}