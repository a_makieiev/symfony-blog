<?php
/**
 * Created by PhpStorm.
 * User: makeey
 * Date: 26.03.18
 * Time: 22:47
 */

namespace App\Controller;


use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;


class DefaultController extends AbstractController
{


    /**
     * @Rest\Route("/")
     * @return Response
     */
    public function mainAction()
    {
        return new Response('<html>
        <body>
        <h2><a href="https://documenter.getpostman.com/view/2008597/symfony-blog/RVu1GAEj#f14c7a6a-eeea-f38c-5d28-717788bee541">API DOCS</a></h2>
</body>
</html>');
    }

}