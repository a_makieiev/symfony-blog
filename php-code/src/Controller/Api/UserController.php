<?php
/**
 * Created by PhpStorm.
 * User: makeey
 * Date: 26.03.18
 * Time: 21:16
 */

namespace App\Controller\Api;


use App\Entity\User;
use App\Utils\Validator;
use FOS\RestBundle\Controller\Annotations as FOSRest;
use FOS\RestBundle\View\View;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


class UserController extends Controller
{


    /**
     * Register User.
     * @FOSRest\Post("/user")
     * @param Request $request
     * @return View
     * @throws \Exception
     */
    public function postUserAction(Request $request)
    {
        $username = Validator::validateParam($request->request->get('username'), 'username');
        $email = Validator::validateParam($request->request->get('email'), 'email');
        $password = Validator::validateParam($request->request->get('password'), 'password');
        $user = User::createUser($username,
            $email,
            $password);
        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();
        return View::create($user, Response::HTTP_CREATED, []);
    }

}