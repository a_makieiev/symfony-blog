<?php
/**
 * Created by PhpStorm.
 * User: makeey
 * Date: 26.03.18
 * Time: 21:00
 */

namespace App\Controller\Api;


use App\Entity\Post;
use App\Repository\CategoryRepository;
use App\Repository\PostRepository;
use App\Utils\Validator;
use FOS\RestBundle\Controller\Annotations as FOSRest;
use FOS\RestBundle\View\View;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class PostController extends Controller
{

    private $postRepository;
    private $categoryRepository;

    public function __construct(PostRepository $postRepository, CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
        $this->postRepository = $postRepository;
    }

    /**
     * Get all posts
     * @FOSRest\Get("/posts")
     * @return View
     */
    public function getArticlesAction()
    {

        $postsOriginal = $this->postRepository->findall();
        $posts = [];
        foreach ($postsOriginal as $postOriginal) {
            $posts[] = $postOriginal->publicNormalize();
        }
        return View::create($posts, Response::HTTP_OK, []);

    }


    /**
     * Get one post
     * @FOSRest\Get("/post/{id}", requirements={"id": "[1-9]\d*"})
     * @param int $id
     * @return View
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getArticleAction(int $id)
    {
        $post = $this->postRepository->findById($id);
        return View::create($post->publicNormalize(), Response::HTTP_OK, []);
    }


    /**
     * Get posts by category
     * @FOSRest\Get("/posts/category/{id}", requirements={"id": "[1-9]\d*"})
     * @param int $id
     * @return View
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getArticleByCategoryAction(int $id)
    {

        $category = $this->categoryRepository->findById($id);
        if(!$category) throw new NotFoundHttpException('category not found');
        $posts_original = $this->postRepository->findByCategoryId($id);
        $posts = [];
        /** @var Post $post_original */
        if(!$posts_original) throw new NotFoundHttpException('Posts not found');
        foreach ($posts_original as $post_original){
            $posts[] = $post_original->publicNormalize();
        }
        return View::create($posts,Response::HTTP_OK,[]);

    }
    /**
     * Create post
     * @FOSRest\Post("/api/post", requirements={"id": "[1-9]\d*"})
     *
     * @param Request $request
     * @return View
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function postCreateAction(Request $request)
    {

        $title = Validator::validateParam($request->request->get('title'), 'title');
        $content = Validator::validateParam($request->request->get('content'), 'content');
        $user = $this->getUser();
        $categoryId = Validator::validateParam($request->request->get('category'), 'category');
        $category = $this->categoryRepository->findById($categoryId);
        if(!$category) throw new  NotFoundHttpException('category not found');
        $post = new Post();
        $post->setTitle($title);
        $post->setPublishedAt();
        $post->setContent($content);
        $post->setAuthor($user);
        $post->setCategory($category);
        $em = $this->getDoctrine()->getManager();
        $em->persist($post);
        $em->flush();
        return View::create($post->publicNormalize(), Response::HTTP_OK, []);

    }


    /**
     * Delete post
     * @FOSRest\Get("/api/post/delete/{id}", requirements={"id": "[1-9]\d*"})
     *
     * @param int $id
     * @return View
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function postPostDeleteAction(int $id)
    {
        $post = $this->postRepository->findById($id);
        $em = $this->getDoctrine()->getManager();
        $em->remove($post);
        $em->flush();
        $data_response = [
            'status' => 'OK',
            'massage' => $post->getTitle().' '.'was been remove'
        ];
        return View::create($data_response, Response::HTTP_OK, []);

    }


    /**
     * Edit post
     * @FOSRest\Post("/api/post/edit")
     * @param Request $request
     * @return View
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function postEditPostAction(Request $request)
    {
        $id = Validator::validateParam($request->request->get('id'),'id');
        $post = $this->postRepository->findById($id);
        if(!$post) { throw new NotFoundHttpException('Post not found'); }
        $title = $request->request->get('title');
        if($title) { $post->setTitle($title); }
        $content = $request->request->get('content');
        if($content) { $post->setContent($content); }
        $category = $request->request->get('category');
        if($category){
            $category_obj = $this->categoryRepository->findById($id);
            if(!$category_obj) throw new NotFoundHttpException('Category not found');
            $post->setCategory($category_obj);
        }
        $em = $this->getDoctrine()->getManager();
        $em->flush();
        return View::create($post->publicNormalize(), Response::HTTP_OK, []);

    }

}