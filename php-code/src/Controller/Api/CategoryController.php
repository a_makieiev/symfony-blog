<?php
/**
 * Created by PhpStorm.
 * User: makeey
 * Date: 28.03.18
 * Time: 22:30
 */

namespace App\Controller\Api;


use App\Entity\Category;
use App\Repository\CategoryRepository;
use App\Repository\PostRepository;
use App\Utils\Validator;
use FOS\RestBundle\View\View;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as FOSRest;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


class CategoryController extends Controller
{

    private $categoryRepository;
    private $postRepository;
    public function __construct(CategoryRepository $categoryRepository, PostRepository $postRepository)
    {
        $this->postRepository = $postRepository;
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * Create a category.
     * @FOSRest\Post("/api/category")
     * @param Request $request
     * @return View
     */
    public function postCreateAction(Request $request){
        $title = Validator::validateParam($request->request->get('title'),'Title Category');
        $category = new Category($title);
        $em = $this->getDoctrine()->getManager();
        $em->persist($category);
        $em->flush();
        return View::create($category,Response::HTTP_OK,[]);
    }

    /**
     * Delete category
     * @FOSRest\Get("/api/category/delete/{id}", requirements={"id": "[1-9]\d*"})
     * @param int $id
     * @return View
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getDeleteAction(int $id){
        $category = $this->categoryRepository->findById($id);
        if(!$category) throw new NotFoundHttpException('Category not found');
        $em = $this->getDoctrine()->getManager();
        $posts = $this->postRepository->findByCategoryId($category->getId());
        foreach ($posts as $post) {
            $em->remove($post);
        }
        $em->remove($category);
        $em->flush();
        $responseData = [
            'status' => 'OK',
            'massage' => $category->getTitle().' was been deleted'
        ];
        return View::create($responseData,Response::HTTP_OK,[]);
    }

    /**
     * Edit category
     * @FOSRest\Post("/api/category/edit")
     * @param Request $request
     * @return View
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getEditAction(Request $request){
        $id = Validator::validateParam($request->request->get('id'),'id');
        $title = Validator::validateParam($request->request->get('title'),'title');
        $category = $this->categoryRepository->findById($id);
        if(!$category) throw new NotFoundHttpException('Category not found');
        $category->setTitle($title);
        $em = $this->getDoctrine()->getManager();
        $em->flush();
        return View::create($category,Response::HTTP_OK,[]);
    }

    /**
     * Get all categories
     * @FOSRest\Get("/categories")
     * @param Request $request
     * @return View
     */
    public function getCategoriesAction(Request $request){
        $categories = $this->categoryRepository->findAll();
        return View::create($categories,Response::HTTP_OK,[]);
    }

    /**
     * Get one category
     * @FOSRest\Get("/category/{id}", requirements={"id": "[1-9]\d*"})
     * @param int $id
     * @return View
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getCategoryAction(int $id){
        $category = $this->categoryRepository->findById($id);
        if(!$category) throw new NotFoundHttpException('Category not found');
        return View::create($category,Response::HTTP_OK,[]);
    }

}