<?php
/**
 * Created by PhpStorm.
 * User: makeey
 * Date: 28.03.18
 * Time: 20:56
 */

namespace App\Controller\Api;


use App\Entity\User;
use App\Repository\UserRepository;
use App\Utils\Validator;
use FOS\RestBundle\View\View;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

use FOS\RestBundle\Controller\Annotations as FOSRest;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;

class AuthController extends Controller
{

    private $userRepository;
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;

    }

    /**
     * @FOSRest\Post("/login")
     *
     * @param Request $request
     * @return View
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function postLoginAction(Request $request){
        $username = Validator::validateParam($request->request->get('username'),'username');
        $password = Validator::validateParam($request->request->get('password'),'password');
        $user = $this->userRepository->findByUsername($username);
        if(!($user instanceof User)){
            throw new NotFoundHttpException('User not found');
        }
        if(!$user->validatePassword($password)){
            throw new UnauthorizedHttpException('','Inavalid password');
        }
        $response_date = [
          'token' => $user->getToken()
        ];
        return View::create($response_date,Response::HTTP_OK,[]);
    }




}