<?php
/**
 * Created by PhpStorm.
 * User: makeey
 * Date: 28.03.18
 * Time: 20:57
 */

namespace App\Utils;


use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class Validator
{

    /**
     * @param $param
     * @param $paramName
     * @return mixed
     */
    public static function validateParam($param, $paramName)
    {
        if (empty($param)) {
            throw new BadRequestHttpException($paramName . ' is require');
        }
        return $param;
    }

}