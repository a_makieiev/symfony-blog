<?php
/**
 * Created by PhpStorm.
 * User: makeey
 * Date: 26.03.18
 * Time: 13:04
 */

namespace App\Tests\Entity;



use App\Entity\User;
use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{

    public function testEmail(){
        $user = new User();
        $email = 'makeey@gmail.com';
        $user->setEmail($email);
        $this->assertEquals($email,$user->getEmail());
    }
    public function testFailEmail(){
        $user = new User();
        $email = 'makeey';
        $user->setEmail($email);
        $this->assertEquals(null,$user->getEmail());
    }
    public function testCreateUser(){
        $username = 'makeey';
        $password = '17506396Qq';
        $email = 'makeey97@gmail.com';
        $user = User::createUser($username,$email,$password);
        $this->assertEquals($username,$user->getUsername());
        $this->assertEquals($email,$user->getEmail());
        $this->assertEquals($password,$user->getPassword());
        $this->assertEquals(['ROLE_USER'],$user->getRoles());
        $this->assertNotEmpty($user->getToken());
    }
}